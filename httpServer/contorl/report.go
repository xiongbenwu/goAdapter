package contorl

import (
	"encoding/json"
	"fmt"
	"goAdapter/report/mqttAliyun"
	mqttEmqx "goAdapter/report/mqttEMQX"
	"goAdapter/report/mqttHuawei"
	"goAdapter/report/mqttThingsBoard"
	"goAdapter/setting"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

func ApiSetReportGWParam(context *gin.Context) {

	type ReportServiceTemplate struct {
		ServiceName string
		IP          string
		Port        string
		ReportTime  int
		Protocol    string
		Param       interface{}
	}

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	fmt.Println(string(bodyBuf[:n]))

	var Param json.RawMessage
	param := ReportServiceTemplate{
		Param: &Param,
	}

	err := json.Unmarshal(bodyBuf[:n], &param)
	if err != nil {
		fmt.Println("param json unMarshall err,", err)

		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)

		context.String(http.StatusOK, string(sJson))
		return
	}

	switch param.Protocol {
	case "Aliyun.MQTT":
		ReportServiceGWParamAliyun := mqttAliyun.ReportServiceGWParamAliyunTemplate{}
		if err := json.Unmarshal(bodyBuf[:n], &ReportServiceGWParamAliyun); err != nil {
			fmt.Println("ReportServiceGWParamAliyun json unMarshall err,", err)
		}
		mqttAliyun.ReportServiceParamListAliyun.AddReportService(ReportServiceGWParamAliyun)
	case "EMQX.MQTT":
		ReportServiceGWParamEmqx := mqttEmqx.ReportServiceGWParamEmqxTemplate{}
		if err := json.Unmarshal(bodyBuf[:n], &ReportServiceGWParamEmqx); err != nil {
			fmt.Println("ReportServiceGWParamEmqx json unMarshall err,", err)
		}
		mqttEmqx.ReportServiceParamListEmqx.AddReportService(ReportServiceGWParamEmqx)
	case "ThingsBoard.MQTT":
		ReportServiceGWParamThingsBoard := mqttThingsBoard.ReportServiceGWParamThingsBoardTemplate{}
		if err := json.Unmarshal(bodyBuf[:n], &ReportServiceGWParamThingsBoard); err != nil {
			fmt.Println("ReportServiceGWParamThingsBoard json unMarshall err,", err)
		}
		mqttThingsBoard.ReportServiceParamListThingsBoard.AddReportService(ReportServiceGWParamThingsBoard)
	case "Huawei.MQTT":
		ReportServiceGWParamHuawei := mqttHuawei.ReportServiceGWParamHuaweiTemplate{}
		if err := json.Unmarshal(bodyBuf[:n], &ReportServiceGWParamHuawei); err != nil {
			fmt.Println("ReportServiceGWParamAliyun json unMarshall err,", err)
		}
		mqttHuawei.ReportServiceParamListHuawei.AddReportService(ReportServiceGWParamHuawei)
	default:
		setting.ZAPS.Errorf("unknown param.Protocol")
		aParam.Code = "1"
		aParam.Message = "unknown param.Protocol"
		aParam.Data = ""
		sJson, _ := json.Marshal(aParam)

		context.String(http.StatusOK, string(sJson))
		return
	}

	aParam.Code = "0"
	aParam.Message = ""
	aParam.Data = ""
	sJson, _ := json.Marshal(aParam)

	context.String(http.StatusOK, string(sJson))
}

func ApiGetReportGWParam(context *gin.Context) {

	type ReportServiceTemplate struct {
		ServiceName string
		IP          string
		Port        string
		ReportTime  int
		CommStatus  string
		Protocol    string
		Param       interface{}
	}

	aParam := struct {
		Code    string                  `json:"Code"`
		Message string                  `json:"Message"`
		Data    []ReportServiceTemplate `json:"Data"`
	}{
		Data: make([]ReportServiceTemplate, 0),
	}

	aParam.Code = "0"
	aParam.Message = ""

	for _, v := range mqttAliyun.ReportServiceParamListAliyun.ServiceList {

		ReportService := ReportServiceTemplate{}
		ReportService.ServiceName = v.GWParam.ServiceName
		ReportService.IP = v.GWParam.IP
		ReportService.Port = v.GWParam.Port
		ReportService.ReportTime = v.GWParam.ReportTime
		ReportService.Protocol = v.GWParam.Protocol
		ReportService.Param = v.GWParam.Param
		ReportService.CommStatus = v.GWParam.ReportStatus

		aParam.Data = append(aParam.Data, ReportService)
	}

	for _, v := range mqttEmqx.ReportServiceParamListEmqx.ServiceList {

		ReportService := ReportServiceTemplate{}
		ReportService.ServiceName = v.GWParam.ServiceName
		ReportService.IP = v.GWParam.IP
		ReportService.Port = v.GWParam.Port
		ReportService.ReportTime = v.GWParam.ReportTime
		ReportService.Protocol = v.GWParam.Protocol
		ReportService.Param = v.GWParam.Param
		ReportService.CommStatus = v.GWParam.ReportStatus

		aParam.Data = append(aParam.Data, ReportService)
	}

	for _, v := range mqttThingsBoard.ReportServiceParamListThingsBoard.ServiceList {

		ReportService := ReportServiceTemplate{}
		ReportService.ServiceName = v.GWParam.ServiceName
		ReportService.IP = v.GWParam.IP
		ReportService.Port = v.GWParam.Port
		ReportService.ReportTime = v.GWParam.ReportTime
		ReportService.Protocol = v.GWParam.Protocol
		ReportService.Param = v.GWParam.Param
		ReportService.CommStatus = v.GWParam.ReportStatus

		aParam.Data = append(aParam.Data, ReportService)
	}

	for _, v := range mqttHuawei.ReportServiceParamListHuawei.ServiceList {

		ReportService := ReportServiceTemplate{}
		ReportService.ServiceName = v.GWParam.ServiceName
		ReportService.IP = v.GWParam.IP
		ReportService.Port = v.GWParam.Port
		ReportService.ReportTime = v.GWParam.ReportTime
		ReportService.Protocol = v.GWParam.Protocol
		ReportService.Param = v.GWParam.Param
		ReportService.CommStatus = v.GWParam.ReportStatus

		aParam.Data = append(aParam.Data, ReportService)
	}

	sJson, _ := json.Marshal(aParam)

	context.String(http.StatusOK, string(sJson))
}

func ApiDeleteReportGWParam(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	fmt.Println(string(bodyBuf[:n]))

	param := struct {
		ServiceName string
	}{}

	err := json.Unmarshal(bodyBuf[:n], &param)
	if err != nil {
		fmt.Println("param json unMarshall err,", err)

		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)

		context.String(http.StatusOK, string(sJson))
		return
	}

	//查看Aliyun
	for _, v := range mqttAliyun.ReportServiceParamListAliyun.ServiceList {
		if v.GWParam.ServiceName == param.ServiceName {
			mqttAliyun.ReportServiceParamListAliyun.DeleteReportService(param.ServiceName)

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)

			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	//查看Emqx
	for _, v := range mqttEmqx.ReportServiceParamListEmqx.ServiceList {
		if v.GWParam.ServiceName == param.ServiceName {
			mqttEmqx.ReportServiceParamListEmqx.DeleteReportService(param.ServiceName)

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)

			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	//查看ThingsBoard
	for _, v := range mqttThingsBoard.ReportServiceParamListThingsBoard.ServiceList {
		if v.GWParam.ServiceName == param.ServiceName {
			mqttThingsBoard.ReportServiceParamListThingsBoard.DeleteReportService(param.ServiceName)

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)

			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for _, v := range mqttHuawei.ReportServiceParamListHuawei.ServiceList {
		if v.GWParam.ServiceName == param.ServiceName {
			mqttHuawei.ReportServiceParamListHuawei.DeleteReportService(param.ServiceName)

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)

			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	aParam.Code = "1"
	aParam.Message = "serviceName is not exist"
	aParam.Data = ""
	sJson, _ := json.Marshal(aParam)

	context.String(http.StatusOK, string(sJson))
}

func ApiSetReportNodeWParam(context *gin.Context) {

	type ReportServiceNodeTemplate struct {
		ServiceName       string
		CollInterfaceName string
		Addr              string
		Name              string
		CommStatus        string
		ReportStatus      string
		Protocol          string
		Param             interface{}
	}

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	fmt.Println(string(bodyBuf[:n]))

	var Param json.RawMessage
	param := ReportServiceNodeTemplate{
		Param: &Param,
	}

	err := json.Unmarshal(bodyBuf[:n], &param)
	if err != nil {
		fmt.Println("param json unMarshall err,", err)

		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)

		context.String(http.StatusOK, string(sJson))
		return
	}

	switch param.Protocol {
	case "Aliyun.MQTT":
		ReportServiceNodeParamAliyun := mqttAliyun.ReportServiceNodeParamAliyunTemplate{}
		if err := json.Unmarshal(bodyBuf[:n], &ReportServiceNodeParamAliyun); err != nil {
			setting.ZAPS.Errorf("ReportServiceNodeParamAliyun json unMarshall err,", err)
		}
		for _, v := range mqttAliyun.ReportServiceParamListAliyun.ServiceList {
			if v.GWParam.ServiceName == param.ServiceName {
				v.AddReportNode(ReportServiceNodeParamAliyun)
			}
		}
		setting.ZAPS.Debugf("ParamListAliyun %v", mqttAliyun.ReportServiceParamListAliyun.ServiceList)
	case "EMQX.MQTT":
		ReportServiceNodeParamEmqx := mqttEmqx.ReportServiceNodeParamEmqxTemplate{}
		if err := json.Unmarshal(bodyBuf[:n], &ReportServiceNodeParamEmqx); err != nil {
			setting.ZAPS.Errorf("ReportServiceNodeParamEmqx json unMarshall err,", err)
		}
		for _, v := range mqttEmqx.ReportServiceParamListEmqx.ServiceList {
			if v.GWParam.ServiceName == param.ServiceName {
				v.AddReportNode(ReportServiceNodeParamEmqx)
			}
		}
		setting.ZAPS.Debugf("ParamListEmqx %v", mqttEmqx.ReportServiceParamListEmqx.ServiceList)
	case "ThingsBoard.MQTT":
		ReportServiceNodeParamThingsBoard := mqttThingsBoard.ReportServiceNodeParamThingsBoardTemplate{}
		if err := json.Unmarshal(bodyBuf[:n], &ReportServiceNodeParamThingsBoard); err != nil {
			setting.ZAPS.Errorf("ReportServiceNodeParamThingsBoard json unMarshall err,", err)
		}
		for _, v := range mqttThingsBoard.ReportServiceParamListThingsBoard.ServiceList {
			if v.GWParam.ServiceName == param.ServiceName {
				v.AddReportNode(ReportServiceNodeParamThingsBoard)
			}
		}
		setting.ZAPS.Debugf("ParamListThingsBoard %v", mqttThingsBoard.ReportServiceParamListThingsBoard.ServiceList)
	case "Huawei.MQTT":
		ReportServiceNodeParamHuawei := mqttHuawei.ReportServiceNodeParamHuaweiTemplate{}
		if err := json.Unmarshal(bodyBuf[:n], &ReportServiceNodeParamHuawei); err != nil {
			setting.ZAPS.Errorf("ReportServiceNodeParamHuawei json unMarshall err,", err)
		}
		for _, v := range mqttHuawei.ReportServiceParamListHuawei.ServiceList {
			if v.GWParam.ServiceName == param.ServiceName {
				v.AddReportNode(ReportServiceNodeParamHuawei)
			}
		}
	default:
		setting.ZAPS.Errorf("unknown param.Protocol")
		aParam.Code = "1"
		aParam.Message = "unknown param.Protocol"
		aParam.Data = ""
		sJson, _ := json.Marshal(aParam)

		context.String(http.StatusOK, string(sJson))
		return
	}

	aParam.Code = "0"
	aParam.Message = ""
	aParam.Data = ""
	sJson, _ := json.Marshal(aParam)

	context.String(http.StatusOK, string(sJson))
}

func ApiBatchAddReportNodeParam(context *gin.Context) {

	aParam := struct {
		Code    string
		Message string
		Data    string
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	// 获取文件头
	file, err := context.FormFile("file")
	if err != nil {
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	fileName := exeCurDir + "/config/" + file.Filename

	//保存文件到服务器本地
	if err := context.SaveUploadedFile(file, fileName); err != nil {
		aParam.Code = "1"
		aParam.Message = "save File Error"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	result := setting.LoadCsvCfg(fileName, 1, 2, 0)
	if result == nil {
		return
	}

	for _, record := range result.Records {
		protocol := record.GetString("Protocol")
		setting.ZAPS.Debugf("protocal %v\n", protocol)
		switch protocol {
		case "Aliyun.MQTT":
			{
				ReportServiceNodeParamAliyun := mqttAliyun.ReportServiceNodeParamAliyunTemplate{}
				ReportServiceNodeParamAliyun.ServiceName = record.GetString("ServiceName")
				ReportServiceNodeParamAliyun.CollInterfaceName = record.GetString("CollInterfaceName")
				ReportServiceNodeParamAliyun.Name = record.GetString("Name")
				ReportServiceNodeParamAliyun.Addr = record.GetString("Addr")
				ReportServiceNodeParamAliyun.Protocol = record.GetString("Protocol")
				ReportServiceNodeParamAliyun.Param.ProductKey = record.GetString("ProductKey")
				ReportServiceNodeParamAliyun.Param.DeviceName = record.GetString("DeviceName")
				ReportServiceNodeParamAliyun.Param.DeviceSecret = record.GetString("DeviceSecret")

				for _, v := range mqttAliyun.ReportServiceParamListAliyun.ServiceList {
					if v.GWParam.ServiceName == ReportServiceNodeParamAliyun.ServiceName {
						v.AddReportNode(ReportServiceNodeParamAliyun)
					}
				}
			}
		}
	}

	aParam.Code = "0"
	aParam.Message = ""

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiGetReportNodeWParam(context *gin.Context) {

	type ReportServiceNodeTemplate struct {
		ServiceName       string
		CollInterfaceName string
		Name              string
		Addr              string
		CommStatus        string
		ReportStatus      string
		Protocol          string
		Param             interface{}
	}

	aParam := struct {
		Code    string                      `json:"Code"`
		Message string                      `json:"Message"`
		Data    []ReportServiceNodeTemplate `json:"Data"`
	}{
		Data: make([]ReportServiceNodeTemplate, 0),
	}

	ServiceName := context.Query("ServiceName")

	for _, v := range mqttAliyun.ReportServiceParamListAliyun.ServiceList {
		if v.GWParam.ServiceName == ServiceName {
			ReportServiceNode := ReportServiceNodeTemplate{}
			for _, d := range v.NodeList {
				ReportServiceNode.ServiceName = d.ServiceName
				ReportServiceNode.CollInterfaceName = d.CollInterfaceName
				ReportServiceNode.Name = d.Name
				ReportServiceNode.Addr = d.Addr
				ReportServiceNode.Protocol = d.Protocol
				ReportServiceNode.CommStatus = d.CommStatus
				ReportServiceNode.ReportStatus = d.ReportStatus
				ReportServiceNode.Param = d.Param
				aParam.Data = append(aParam.Data, ReportServiceNode)
			}
			aParam.Code = "0"
			aParam.Message = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for _, v := range mqttEmqx.ReportServiceParamListEmqx.ServiceList {
		if v.GWParam.ServiceName == ServiceName {
			ReportServiceNode := ReportServiceNodeTemplate{}
			for _, d := range v.NodeList {
				ReportServiceNode.ServiceName = d.ServiceName
				ReportServiceNode.CollInterfaceName = d.CollInterfaceName
				ReportServiceNode.Name = d.Name
				ReportServiceNode.Addr = d.Addr
				ReportServiceNode.Protocol = d.Protocol
				ReportServiceNode.CommStatus = d.CommStatus
				ReportServiceNode.ReportStatus = d.ReportStatus
				ReportServiceNode.Param = d.Param
				aParam.Data = append(aParam.Data, ReportServiceNode)
			}
			aParam.Code = "0"
			aParam.Message = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for _, v := range mqttThingsBoard.ReportServiceParamListThingsBoard.ServiceList {
		if v.GWParam.ServiceName == ServiceName {
			ReportServiceNode := ReportServiceNodeTemplate{}
			for _, d := range v.NodeList {
				ReportServiceNode.ServiceName = d.ServiceName
				ReportServiceNode.CollInterfaceName = d.CollInterfaceName
				ReportServiceNode.Name = d.Name
				ReportServiceNode.Addr = d.Addr
				ReportServiceNode.Protocol = d.Protocol
				ReportServiceNode.CommStatus = d.CommStatus
				ReportServiceNode.ReportStatus = d.ReportStatus
				ReportServiceNode.Param = d.Param
				aParam.Data = append(aParam.Data, ReportServiceNode)
			}
			aParam.Code = "0"
			aParam.Message = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for _, v := range mqttHuawei.ReportServiceParamListHuawei.ServiceList {
		if v.GWParam.ServiceName == ServiceName {
			ReportServiceNode := ReportServiceNodeTemplate{}
			for _, d := range v.NodeList {
				ReportServiceNode.ServiceName = d.ServiceName
				ReportServiceNode.CollInterfaceName = d.CollInterfaceName
				ReportServiceNode.Name = d.Name
				ReportServiceNode.Addr = d.Addr
				ReportServiceNode.Protocol = d.Protocol
				ReportServiceNode.CommStatus = d.CommStatus
				ReportServiceNode.ReportStatus = d.ReportStatus
				ReportServiceNode.Param = d.Param
				aParam.Data = append(aParam.Data, ReportServiceNode)
			}
			aParam.Code = "0"
			aParam.Message = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	aParam.Code = "1"
	aParam.Message = "ServiceName is not correct"
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiDeleteReportNodeWParam(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	fmt.Println(string(bodyBuf[:n]))

	param := struct {
		ServiceName       string
		CollInterfaceName string
		Addr              string
	}{}

	err := json.Unmarshal(bodyBuf[:n], &param)
	if err != nil {
		fmt.Println("param json unMarshall err,", err)

		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)

		context.String(http.StatusOK, string(sJson))
		return
	}

	//查看Aliyun
	for _, v := range mqttAliyun.ReportServiceParamListAliyun.ServiceList {
		for _, n := range v.NodeList {
			if (n.ServiceName == param.ServiceName) &&
				(n.CollInterfaceName == param.CollInterfaceName) &&
				(n.Addr == param.Addr) {

				v.DeleteReportNode(n.Name)

				aParam.Code = "0"
				aParam.Message = ""
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)

				context.String(http.StatusOK, string(sJson))
				return
			}
		}
	}

	//查看Emqx
	for _, v := range mqttEmqx.ReportServiceParamListEmqx.ServiceList {
		for _, n := range v.NodeList {
			if (n.ServiceName == param.ServiceName) &&
				(n.CollInterfaceName == param.CollInterfaceName) &&
				(n.Addr == param.Addr) {

				v.DeleteReportNode(n.Name)

				aParam.Code = "0"
				aParam.Message = ""
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)

				context.String(http.StatusOK, string(sJson))
				return
			}
		}
	}

	//查看ThingsBoard
	for _, v := range mqttThingsBoard.ReportServiceParamListThingsBoard.ServiceList {
		for _, n := range v.NodeList {
			if (n.ServiceName == param.ServiceName) &&
				(n.CollInterfaceName == param.CollInterfaceName) &&
				(n.Addr == param.Addr) {

				v.DeleteReportNode(n.Name)

				aParam.Code = "0"
				aParam.Message = ""
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)

				context.String(http.StatusOK, string(sJson))
				return
			}
		}
	}

	for _, v := range mqttHuawei.ReportServiceParamListHuawei.ServiceList {
		for _, n := range v.NodeList {
			if (n.ServiceName == param.ServiceName) &&
				(n.CollInterfaceName == param.CollInterfaceName) &&
				(n.Addr == param.Addr) {

				v.DeleteReportNode(n.Name)

				aParam.Code = "0"
				aParam.Message = ""
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)

				context.String(http.StatusOK, string(sJson))
				return
			}
		}
	}

	aParam.Code = "1"
	aParam.Message = "node is not exist"
	sJson, _ := json.Marshal(aParam)

	context.String(http.StatusOK, string(sJson))
}
