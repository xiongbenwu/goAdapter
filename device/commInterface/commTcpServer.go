package commInterface

import (
	"encoding/json"
	"goAdapter/setting"
	"goAdapter/utils"
	"net"
	"strings"
)

type TcpServerInterfaceParam struct {
	Port     string `json:"Port"`     //监听端口
	Timeout  string `json:"Timeout"`  //通信超时
	Interval string `json:"Interval"` //通信间隔
}

type CommunicationTcpServerTemplate struct {
	Name  string                  `json:"Name"`  //接口名称
	Type  string                  `json:"Type"`  //接口类型,比如serial,TcpClient,TcpServer,udp,http
	Param TcpServerInterfaceParam `json:"Param"` //接口参数
	Conn  net.Conn                `json:"-"`     //通信句柄
}

var CommunicationTcpServerMap = make([]*CommunicationTcpServerTemplate, 0)

func (c *CommunicationTcpServerTemplate) Open() bool {
	//listener, err := net.Listen("tcp", c.Param.IP+":"+c.Param.Port)
	listener, err := net.Listen("tcp", "0.0.0.0:"+c.Param.Port)
	if err != nil {
		setting.ZAPS.Errorf("通信TcpServer[%s]打开失败 %v", c.Name, err)
		return false
	}
	setting.ZAPS.Debugf("通信TcpServer[%s]打开成功", c.Name)

	go c.Accept(listener)

	return true
}

func (c *CommunicationTcpServerTemplate) Accept(listener net.Listener) bool {

	for {
		conn, err := listener.Accept()
		if err != nil {
			setting.ZAPS.Errorf("通信TcpServer[%s]监听失败 %v", c.Name, err)
			continue
		}
		setting.ZAPS.Debugf("通信TcpServer[%s]接收链接成功", c.Name)
		c.Conn = conn
	}
}

func (c *CommunicationTcpServerTemplate) Close() bool {
	if c.Conn != nil {
		err := c.Conn.Close()
		if err != nil {
			setting.ZAPS.Errorf("通信TcpServer[%s]关闭失败 %v", c.Name, err)
			return false
		}
		setting.ZAPS.Debug("通信TcpServer[%s]关闭成功", c.Name)
	}
	c.Conn = nil
	return true
}

func (c *CommunicationTcpServerTemplate) WriteData(data []byte) int {

	if c.Conn != nil {
		cnt, err := c.Conn.Write(data)
		if err != nil {
			//setting.ZAPS.Errorf("%s TcpServer write err %v", c.Name, err)
			return 0
		}
		return cnt
	}
	return 0
}

func (c *CommunicationTcpServerTemplate) ReadData(data []byte) int {

	if c.Conn != nil {
		cnt, err := c.Conn.Read(data)
		//setting.ZAPS.Debugf("%s,TcpServer read data cnt %v", c.Name, cnt)
		if err != nil {
			//setting.ZAPS.Errorf("%s,TcpServer read err,%v", c.Name, err)
			return 0
		}
		return cnt
	}
	return 0
}

func (c *CommunicationTcpServerTemplate) GetName() string {
	return c.Name
}

func (c *CommunicationTcpServerTemplate) GetTimeOut() string {
	return c.Param.Timeout
}

func (c *CommunicationTcpServerTemplate) GetInterval() string {
	return c.Param.Interval
}

func (c *CommunicationTcpServerTemplate) GetType() int {
	return CommTypeTcpServer
}

func ReadCommTcpServerInterfaceListFromJson() bool {

	data, err := utils.FileRead("/selfpara/commTcpServerInterface.json")
	if err != nil {
		if strings.Contains(err.Error(), "no such directory") {
			setting.ZAPS.Debug("打开通信接口[TcpServer]通信接口配置json文件失败")
		} else {
			setting.ZAPS.Debugf("打开通信接口[TcpServer]通信接口配置json文件失败 %v", err)
		}
		return false
	}
	err = json.Unmarshal(data, &CommunicationTcpServerMap)
	if err != nil {
		setting.ZAPS.Errorf("通信接口[TcpServer]通信接口配置json文件格式化失败")
		return false
	}
	setting.ZAPS.Debugf("打开通信接口[TcpServer]通信接口配置json文件成功")
	return true
}

func WriteCommTcpServerInterfaceListToJson() {

	utils.DirIsExist("./selfpara")

	sJson, _ := json.Marshal(CommunicationTcpServerMap)
	err := utils.FileWrite("/selfpara/commTcpServerInterface.json", sJson)
	if err != nil {
		setting.ZAPS.Errorf("写入TcpServer通信接口配置json文件 %s %v", "失败", err)
		return
	}
	setting.ZAPS.Infof("写入TcpServer通信接口配置json文件 %s", "成功")
}
